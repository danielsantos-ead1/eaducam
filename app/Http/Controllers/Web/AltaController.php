<?php

namespace App\Http\Controllers\Web;

use App\Utils\G2;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;

class AltaController extends BaseController
{
    public function index()
    {
        $st_texto = Input::get('st_texto');
        $id_categoria = Input::get('id_categoria');
        $gestor = new G2();
        $categorias = $gestor->listarCategorias(array('st_texto' => $st_texto, 'id_categoria'=>$id_categoria));
        return view('alta-performance', [ 'categorias' => $categorias ? $categorias : array() ]);
    }

    public function category($id_categoria, $st_categoria, Request $request)
    {
        $gestor = new G2();
        $categorias = $gestor->listarCategorias(array('id_categoria'=>$id_categoria));
        return view('category', [ 'categoria' => $categorias ? $categorias["key_$id_categoria"] : null ]);
    }


    

    public function courseInscription($id_projetopedagogico, Request $request)
    {
        $gestor = new G2();
        $curso = $gestor->listarProduto(array('id_projetopedagogico'=>$id_projetopedagogico));
        if(!$curso) $curso = array();

        return view('course-inscription', [ 'curso' => $curso ]);
    }

    public function search()
    {

//        $gestor = new G2();
//        $cursos = $gestor->listarCategorias();

        return view('cursos');
    }

       
}

