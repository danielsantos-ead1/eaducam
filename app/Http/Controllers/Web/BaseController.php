<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\Controller;
use App\Utils\G2;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\View;

class BaseController extends Controller
{

    public $entity;

    public function __construct() {

        //die('Site em Manutenção'); // comentar esta linha para editar

        $clearCache = Input::get('clear_cache');
        $gestor = new G2($clearCache);

        $entidade = $gestor->retornarEntidade();
        $dadosFixos = [
            'name'=>'Universidade Candido Mendes',
            'phone'=>'0800 282 5353',
            'phones'=>['0800 111 9090','(61) 3037 9090'],
            'whats' =>'61 98615 9090',
            'email'=>'contato@eaducam.com.br',
            'logo'=>'logo-ucam-pos.png',
            'checkout_url'=>'https://loja.ead1.net/loja/pagamento/iniciar',
//            'checkout_url'=>'http://dev2loja.emgsoft.com.br/loja/pagamento/iniciar',
            'st_chave'=>$gestor->getSt_chave(),
            'address'=> [
                'street'=>'',
                'extra'=>'',
                'number'=>'',
                'zipcode'=>'',
                'district'=>'',
                'city'=>'',
                'state'=>''
            ],
            'address2'=> [
                'street'=>'SCS, Quadra 04, Bloco A',
                'extra'=>'Ed. Mineiro, Sala 406',
                'number'=>'nº 209/217',
                'zipcode'=>'70304-911',
                'district'=>'Asa Sul',
                'city'=>' Brasília',
                'state'=>'DF'
            ],
            'social'=> [
                'twitter' => 'https://twitter.com/ucam_oficial',
                'facebook' => 'https://www.facebook.com/UCAM-Inova-EaD-367778367043543/',
                'gplus' => null,
                'youtube' => null
            ]
        ];

        if($entidade) {

            if(!empty($entidade['st_nomeentidade'])) $dadosFixos['name'] = $entidade['st_nomeentidade'];
            if(!empty($entidade['st_endereco']))     $dadosFixos['address']['street'] = $entidade['st_endereco'];
            if(!empty($entidade['st_complemento']) && !is_array($entidade['st_complemento']))     $dadosFixos['address']['extra'] = $entidade['st_complemento'];
            if(!empty($entidade['nu_numero']))     $dadosFixos['address']['number'] = $entidade['nu_numero'];
            if(!empty($entidade['st_nomemunicipio']))     $dadosFixos['address']['city'] = $entidade['st_nomemunicipio'];
            if(!empty($entidade['sg_uf']))     $dadosFixos['address']['state'] = $entidade['sg_uf'];
            if(!empty($entidade['st_bairro']))     $dadosFixos['address']['district'] = $entidade['st_bairro'];
            if(!empty($entidade['st_cep']))     $dadosFixos['address']['zipcode'] = $entidade['st_cep'];

        }

        $this->entity = $dadosFixos;


        View::share ( 'site', $dadosFixos);

    }

}
