<?php

namespace App\Http\Controllers\Web;

use App\Utils\G2;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

class ContactController extends BaseController
{
    public function index($id_produto = null, Request $request)
    {

        $produto = array();

        if($id_produto){
            $gestor = new G2();
            $produto = $gestor->listarProduto(array('id_produto'=>$id_produto));
        }

        if(!$produto) $produto = array();

        return view('contact', ['message'=> 'Envie-nos uma mensagem. <br>Em breve um dos nossos colaboradores entrará em contato.', 'produto' => $produto]);

    }

    public function send( Request $request )
    {

        if(!$request->get('message')){
            return view('contact', ['message'=> 'Você precisa informar alguma mensagem para o envio!']);
        }

        $subject = "Contado do Site ".$this->entity['name'];
        $message = "Nome: ".$request->get('name')."\r\n";
        $message .= "E-mail: ".$request->get('email')."\r\n";
        $message .= "Telefone: ".$request->get('phoneddd')." - ".$request->get('phone')."\r\n";
        $message .= "Mensagem: ".$request->get('message')."\r\n";
        $headers = 'From: contato@ucam.com' . "\r\n";
        if($request->get('email')){
            $headers .='Reply-To: '.$request->get('email')."\r\n";
        }
        $headers .='X-Mailer: PHP/' . phpversion();

        $to = "elag2002@gmail.com";

        $sended = mail($to,$subject,$message,$headers);

        $id_prevenda = null;

        if($request->input('id_produto')){

            if(!$request->get('phone')){
                return view('contact', ['message'=> 'Você precisa informar o telefone para contato!']);
            }

            $gestor = new G2();
            $id_prevenda = $gestor->criarPrevenda(array('produtos'=>['id_produto'=>$request->input('id_produto')]
            , 'st_nomecompleto'=> $request->get('name')
            , 'st_email'=>$request->get('email')
            , 'nu_dddcelular'=>$request->get('phoneddd')
            , 'nu_telefonecelular'=>$request->get('phone')
            , 'nu_valorliquido'=>$request->get('nu_valorliquido')));

        }

        return view('contact-send', [
            'enviado' =>  $sended,
            'id_prevenda' =>  $id_prevenda
        ]);

    }
}
