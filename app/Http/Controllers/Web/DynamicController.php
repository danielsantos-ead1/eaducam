<?php

namespace App\Http\Controllers\Web;

use App\Utils\G2;
use Illuminate\Http\Request;

class DynamicController extends BaseController
{
    public function index(Request $request)
    {
        return view('404-error');
    }

    public function notFound(){
        return view('404-error');
    }

    public function obrig(){
        return view('obrigado');
    }


}
