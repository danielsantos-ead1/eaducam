<?php

namespace App\Http\Controllers\Web;

use App\Utils\G2;

class IndexController extends BaseController
{
    public function index()
    {
        $gestor = new G2();
        $categorias = $gestor->listarCategorias();
        if(!$categorias){
            $categorias = array();
        }

        $produtos = array();
        foreach ($categorias as $categoria){
            if(!empty($categoria['produtos'])) $produtos = array_merge($produtos, $categoria['produtos']);
        }




//        echo '<pre>'.__FILE__.'('.__LINE__.')';
//        var_dump($newProdutos);
//        exit;
//
//        $produtos = $gestor->listarProdutos();
        if(!$produtos){
            $produtos = array();
        }
        shuffle($produtos);
        $newProdutos = array();
        foreach ($produtos as $chave => $valor) {
            if($chave <= 1) $newProdutos[] = $valor;
        }

        return view('index', [
            'slider' => [
                'items' => [
                    [
                        'title' => null,
                        'image' => '/img/home/slider/banner-site-ds.jpg',
                        'description' => null,
                        'url_text' => null,
                        'url' => '/cursos'
                    ],
                    [
                        'title' => 'UCAM',
                        'image' => '/img/home/slider/slider_image_1.jpg',
                        'description' => 'Diploma de Valor desde 1902',
                        'url_text' => 'Saiba mais',
                        'url' => '/inova-ead'
                    ]
                    
                ]
            ],
            'categorias' => $categorias,
            'produtos' => $newProdutos
        ]);



    }

    public function inovaEad(){
        return view('inova-ead');
    }

}
