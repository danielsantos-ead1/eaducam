<?php

namespace App\Utils;

use GuzzleHttp\Client;
use Illuminate\Support\Facades\Storage;

/**
 * Created by PhpStorm.
 * User: elcio
 * Date: 22/03/2018
 * Time: 14:12
 */
class G2
{


//SetEnv APPLICATION_ID_ENTIDADE "116"
//SetEnv APPLICATION_ST_CHAVE "cc47550478b4233566cde9d3f6f8a1d2d6560ee7"


    /**
     * @var Client
     */
    protected $client;

    protected $id_entidade;

    protected $st_chave;

//    protected $id_entidade = 116;
//    protected $st_chave = 'cc47550478b4233566cde9d3f6f8a1d2d6560ee7';
//    protected $id_entidade = 56;
//    protected $st_chave = 'db03ce611fbcfc142a8607787cd9f4006d5bf158';

    protected $cache_time = 48;

    public function __construct($clearCache = false)
    {

        $this->id_entidade = getenv('APPLICATION_ID_ENTIDADE');
        if(!$this->id_entidade) $this->id_entidade = 116;

        $this->st_chave = getenv('APPLICATION_ST_CHAVE');
        if(!$this->st_chave) $this->st_chave = 'cc47550478b4233566cde9d3f6f8a1d2d6560ee7';

        if($clearCache){
            Storage::disk('local')->deleteDirectory("g2");
            sleep(1);
        }

    }

    /**
     * @return int
     */
    public function getId_entidade()
    {
        return $this->id_entidade;
    }

    /**
     * @return string
     */
    public function getSt_chave()
    {
        return $this->st_chave;
    }



    /**
     * @return Client
     */
    public function getClient(){

        $this->client = new Client([
            'base_uri' => 'http://g2s.ead1.net',
//            'base_uri' => 'http://g2.emgsoft.com.br',
            'timeout' => 30.0
        ]);
        return $this->client;

    }


    /**
     * Retorna os dados do cache
     * echo $oDiff->y.' Years <br/>';
     * echo $oDiff->m.' Months <br/>';
     * echo $oDiff->d.' Days <br/>';
     * echo $oDiff->h.' Hours <br/>';
     * echo $oDiff->i.' Minutes <br/>';
     * echo $oDiff->s.' Seconds <br/>';
     * @param $service
     * @param $uri
     * @return bool|mixed
     */
    protected function _retrieveCache($service, $uri) {

        try {

            $filename = "g2/".$this->getId_entidade()."$service".md5($uri).'.txt';

            $exists = Storage::disk('local')->exists($filename);
            if($exists){

                $time = Storage::disk('local')->lastModified($filename);

                $datetime1 = new \DateTime(date('Y-m-d H:i:s', time()));
                $datetime2 = new \DateTime(date('Y-m-d H:i:s', $time));
                $oDiff = $datetime1->diff($datetime2);
                if($oDiff->h >= $this->cache_time) {
                    Storage::disk('local')->delete($filename);
                } else {
                    $contents = Storage::disk('local')->get($filename);
                    return unserialize($contents);
                }

            }

            return false;

        } catch (\Exception $e) {
            return false;
        }

    }

    /**
     * Salva os dados do cache
     * @param $service
     * @param $uri
     * @param $data
     * @return bool
     */
    protected function _saveCache($service, $uri, $data) {

        try {

            $filename = "g2/".$this->getId_entidade()."$service".md5($uri).'.txt';
            Storage::disk('local')->put($filename, serialize($data));
            return true;

        } catch (\Exception $e) {
            return false;
        }

    }

    public function retornarEntidade()
    {

        try {

            $service = 'entidade';

            $method = 'retornar';

            $params = [
                'id' => $this->id_entidade,
                'chave' => $this->st_chave,
                'method' => $method
            ];

            $uri = "/ws/$service?" . http_build_query($params);

            $cache = $this->_retrieveCache($service, $uri);
            if($cache) return $cache;

            $responseArray = $this->_doGetRequest($uri);
            if (!empty($responseArray[$method]['tipo']) && $responseArray[$method]['tipo'] == 1) {
                $dadosEntidade = reset($responseArray[$method]['mensagem']);
                $this->_saveCache($service, $uri, $dadosEntidade);
                return $dadosEntidade;
            } else {
                return null;
            }

        } catch (\Exception $e) {
            return false;
        }

    }


    public function retornarProduto($id_produto)
    {

        try {

            $service = 'produto';

            $method = 'retornar';

            $params = [
                'id' => $this->id_entidade,
                'chave' => $this->st_chave,
                'method' => $method,
                'parametros' => [
                    'id_entidade' => $this->id_entidade,
                    'id_produto' => $id_produto
                ]
            ];

            $uri = "/ws/$service?" . http_build_query($params);

            $cache = $this->_retrieveCache($service, $uri);
            if($cache) return $cache;

            $response = $this->getClient()->request('GET', $uri);
            $body = str_replace('<head/>', '', trim((string)$response->getBody()));
            $responseArray = json_decode(json_encode(simplexml_load_string($body)), true);

            unset($responseArray['@attributes']);

            if (!empty($responseArray[$method]['tipo']) && $responseArray[$method]['tipo'] == 1) {
                $this->_saveCache($service, $uri, $responseArray[$method]['mensagem']);
                return $responseArray[$method]['mensagem'];
            } else {
                return null;
            }

        } catch (\Exception $e) {
            throw $e;
        }

    }


    public function listarProdutos()
    {

        try {

            $service = 'produto';
            $method = 'listarProduto';

            $params = [
                'id' => $this->id_entidade,
                'chave' => $this->st_chave,
                'method' => $method,
                'parametros' => [
                    'id_entidade' => $this->id_entidade
                ],
                'entidades' => [
                    $this->id_entidade
                ],
                'comprojeto' => 1
            ];

            $uri = "/ws/$service?" . http_build_query($params);
            $cache = $this->_retrieveCache($service, $uri);
            if($cache) return $cache;

            $responseArray = $this->_doGetRequest($uri);

            if (!empty($responseArray[$method])) {
                unset( $responseArray[$method]['status'],  $responseArray[$method]['success']);
                $this->_saveCache($service, $uri, $responseArray[$method]);
                return $responseArray[$method];
            } else {
                return null;
            }

        } catch (\Exception $e) {
            throw $e;
        }

    }

    public function listarProdutosCategorias()
    {

        try {

            $service = 'produto';
            $method = 'listar';

            $params = [
                'id' => $this->id_entidade,
                'chave' => $this->st_chave,
                'method' => $method,
                'parametros' => [
                    'id_entidade' => $this->id_entidade
                ]
            ];

            $uri = '/ws/produto?' . http_build_query($params);

            $cache = $this->_retrieveCache($service, $uri);
            if($cache) return $cache;

            $responseArray = $this->_doGetRequest($uri);

            $dados = null;

            if (!empty($responseArray[$method]['tipo']) && $responseArray[$method]['tipo'] == 1) {
                $dados = $responseArray[$method]['mensagem'];
                $this->_saveCache($service, $uri, $dados);
            }

            return $dados;
        } catch (\Exception $e) {
            throw $e;
        }

    }

    public function listarProdutoMensageiro(array $params = array())
    {

        try {

            $service = 'produto';
            $method = 'listar';
            $params['id_entidade'] = $this->id_entidade;

            $params = [
                'id' => $this->id_entidade,
                'chave' => $this->st_chave,
                'method' => $method,
                'parametros' => $params,
                'entidades' => [$this->id_entidade]

            ];

            $uri = '/ws/produto?' . http_build_query($params);
            $cache = $this->_retrieveCache($service, $uri);
            if($cache) return $cache;

            $responseArray = $this->_doGetRequest($uri);

            if (!empty($responseArray[$method]['tipo']) && $responseArray[$method]['tipo'] == 1) {
                $dados = $responseArray[$method]['mensagem'];
                $this->_saveCache($service, $uri, $dados);
                return $dados;
            } else {
                return null;
            }

        } catch (\Exception $e) {
            throw $e;
        }

    }

    public function listarProduto(array $pars = array())
    {

        try {

            $service = 'produto';
            $method = 'listarProduto';
            $pars['id_entidade'] = $this->id_entidade;

            $params = [
                'id' => $this->id_entidade,
                'chave' => $this->st_chave,
                'method' => $method,
                'parametros' => $pars,
                'entidades' => [$this->id_entidade]
            ];

            $uri = '/ws/'.$service.'?' . http_build_query($params);
            $cache = $this->_retrieveCache($service, $uri);
            if($cache) return $cache;

            $responseArray = $this->_doGetRequest($uri);

            if (!empty($responseArray[$method])) {
                $dados = reset($responseArray[$method]);
                $this->_saveCache($service, $uri, $dados);
                return $dados;
            } else {
                return null;
            }

        } catch (\Exception $e) {
            return false;
        }

    }

    public function criarPrevenda($parametros) {
        try {

            $params = [
                'id' => $this->id_entidade,
                'chave' => $this->st_chave,
                'method' => 'prevenda',
                'parametros' => $parametros
            ];

            $uri = '/ws/venda';

            $responseArray = $this->_doPostRequest($uri, $params);

            if (!empty($responseArray['prevenda']['mensagem']["key_0"]['id_prevenda'])) {
                return $responseArray['prevenda']['mensagem']["key_0"]['id_prevenda'];
            } else {
                return null;
            }

        } catch (\Exception $e) {
            return false;
        }
    }

    public function nullAtArray(&$array) {

        if(is_array($array) && !$array) {
            $array = null;
        } else if(is_array($array)) {
            foreach ($array as $key => $value) {
                $this->nullAtArray($array[$key]);
            }
        } else {
            $array = trim($array);
        }

    }

    protected function _doGetRequest($uri){

        $response = $this->getClient()->request('GET', $uri);
        $body = str_replace('<head/>', '', trim((string)$response->getBody()));
        $responseArray = json_decode(json_encode(simplexml_load_string($body)), true);
        unset($responseArray['@attributes']);
        $this->nullAtArray($responseArray);

        return $responseArray;

    }

    protected function _doPostRequest($uri, $data){

        $response = $this->getClient()->post($uri, [
            'debug' => false,
            'form_params' => $data,
            'headers' => [
                'Content-Type' => 'application/x-www-form-urlencoded',
            ]
        ]);
        $body = str_replace('<head/>', '', trim((string)$response->getBody()));
        $responseArray = json_decode(json_encode(simplexml_load_string($body)), true);
        unset($responseArray['@attributes']);
        $this->nullAtArray($responseArray);

        return $responseArray;

    }


    public function listarCategorias($parameters = array())
    {

        try {

            $service = 'categorias';

            $method = 'retornarCategoriaProdutoImagens';

            $parameters['id_entidade'] = $this->id_entidade;

            $params = [
                'id' => $this->id_entidade,
                'chave' => $this->st_chave,
                'method' => $method,
                'parametros' => $parameters
            ];

            $uri = "/ws/$service?" . http_build_query($params);

            $cache = $this->_retrieveCache($service, $uri);
            if($cache) return $cache;

            $responseArray = $this->_doGetRequest($uri);

            if (!empty($responseArray[$method]['tipo']) && $responseArray[$method]['tipo'] == 1) {
                $categorias = $responseArray[$method]['mensagem'];
                $this->_saveCache($service, $uri, $categorias);
                return $categorias;
            } else {
                return null;
            }

        } catch (\Exception $e) {
            return null;
        }

    }

    public function retornarProjetoPedagogico($id_projetopedagogico)
    {

        try {

            $service = 'curso';

            $method = 'retornar';

            $params = [
                'id' => $this->id_entidade,
                'chave' => $this->st_chave,
                'method' => $method,
                'parametros' => [
                    'id_entidade' => $this->id_entidade,
                    'id_projetopedagogico' => $id_projetopedagogico
                ]
            ];

            $uri = "/ws/$service?" . http_build_query($params);

            $cache = $this->_retrieveCache($service, $uri);
            if($cache) return $cache;

            $responseArray = $this->_doGetRequest($uri);

            if (!empty($responseArray[$method]['tipo']) && $responseArray[$method]['tipo'] == 1) {
                $categorias = $responseArray[$method]['mensagem'];
                $this->_saveCache($service, $uri, $categorias);
                return $categorias;
            } else {
                return null;
            }

        } catch (\Exception $e) {
            return null;
        }

    }

}