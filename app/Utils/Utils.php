<?php

namespace App\Utils;

/**
 * Created by PhpStorm.
 * User: elcio
 * Date: 22/03/2018
 * Time: 14:12
 */
class Utils
{

    /**
     * Parse a SimpleXMLElement object recursively into an Array.
     * Attention: attributes skipped
     *
     *
     * @param $xml The SimpleXMLElement object
     * @param $arr Target array where the values will be stored
     * @return NULL
     */
    public static function convertXmlObjToArr($obj, &$arr)
    {

        try {

            if(is_string($obj)) {
                $obj = simplexml_load_string($obj);
            }

            if(!($obj instanceof \SimpleXMLElement)){
                throw new \Exception("O Objeto informado não é um SimpleXMLElement");
            }

            $children = $obj->children();
            $executed = false;
            foreach ($children as $elementName => $node)
            {
                if( array_key_exists( $elementName , $arr ) )
                {
                    if(array_key_exists( 0 ,$arr[$elementName] ) )
                    {
                        $i = count($arr[$elementName]);
                        self::convertXmlObjToArr ($node, $arr[$elementName][$i]);
                    }
                    else
                    {
                        $tmp = $arr[$elementName];
                        $arr[$elementName] = array();
                        $arr[$elementName][0] = $tmp;
                        $i = count($arr[$elementName]);
                        self::convertXmlObjToArr($node, $arr[$elementName][$i]);
                    }
                }
                else
                {


                    $pos = strpos($elementName, 'key_');
                    if($pos!==false){
                        $chave = explode('_', $elementName);
                        $key = $chave[1];
                    } else {
                        $key = $elementName;
                    }
                    $arr[$key] = array();
                    self::convertXmlObjToArr($node, $arr[$key]);
                }
                $executed = true;
            }
            if(!$executed&&$children->getName()=="")
            {
                $arr = (String)$obj;
            }

            return ;
        } catch (Exception $e) {
            throw $e;
        }


    }


    /**
     * MÃ©todo que pega um Objeto e converte em Array
     * @param object
     */
    public static function object_to_array(  $object){
        $new=NULL;

        if(is_object($object)){
            $object=(array)$object;
            foreach($object as $key => $val) {
                if(is_object($val)){
                    $object[$key] = self::object_to_array($val);
                }

                if(is_string($val)){
                    if(trim($val)==''){
                        unset($object[$key]);
                    } else {
                        $object[$key] = trim($val);
                    }
                }
            }
        }

        if(!$object && $object !== '0'){
            return null;
        }

        if(is_array($object)){
            $new=array();
            foreach($object as $key => $val) {
                $key=preg_replace("/^\\0(.*)\\0/","",$key);
                $key=str_replace("key_","",$key);
                $new[$key] = self::object_to_array($val);
            }
        }else{
            $new = $object;
        }
        return $new;
    }

}