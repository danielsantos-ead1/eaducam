@include('header')
<!-- ERROR CONTENT -->
<div class="mainContent error-content clearfix">
    <div class="container">
        <div class="error-content-top">
            <h1>404</h1>
            <h3>Ops!</h3>
        </div>
        <p>A página que você está procurando não foi encontrada!</p>
        <p>Caso você precise de assitência, entre em contato clicando <a href="/contato">Aqui</a></p>
    </div>
</div>
@include('footer')

