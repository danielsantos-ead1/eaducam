@include('header')


<div class="custom_content custom">
    <div class="container large">
        <div class="row">
            <div class="col-xs-12 col-sm-8 custom_right">
                <div class="single_content_left">
                    <h3>Entre em contato</h3>
                    @if($enviado)
                    <p>Sua mensagem foi enviada com sucesso. Entraremos em contato em breve.@if($id_prevenda) Seu número de protocolo é <strong>{{ $id_prevenda }}</strong> @endif</p>
                        @else
                        <p>Não foi possível enviar sua mensagem. Por favor tente mais tarde ou entre em contato através de nossos outros canais de atendimento. Você pode encontrá-los nesta página.</p>
                    @endif
                </div><!--end single content left-->
            </div>

            <div class="col-xs-12 col-sm-4 custom_left">
                <div class="sidebar">
                    <div class="list_block sidebar_item">
                        <h3>Canais</h3>
                        <ul class="contact_info">
                            <li><i class="fa fa-home"></i>
                                {{ $site['address']['street'] }}, {{ $site['address']['number'] }}<br>
                                {{ $site['address']['extra'] }}, {{ $site['address']['district'] }}<br>
                                {{ $site['address']['city'] }}/{{ $site['address']['state'] }}<br>
                                {{ $site['address']['zipcode'] }}
                            </li>
                            <li><i class="fa fa-home"></i>
                                {{ $site['address2']['street'] }}, {{ $site['address2']['number'] }}<br>
                                {{ $site['address2']['extra'] }}, {{ $site['address2']['district'] }}<br>
                                {{ $site['address2']['city'] }}/{{ $site['address2']['state'] }}<br>
                                {{ $site['address2']['zipcode'] }}
                            </li>

                            @if(!empty($site['email']))
                                <li><i class="fa fa-envelope"></i> <a href="mailto:{{ $site['email']  }}">{{ $site['email']  }}</a></li>
                            @endif

                            @forelse ($site['phones'] as $phone)
                                <li><i class="fa fa-phone"></i>  {{ $phone }}</li>
                            @empty
                                @if(!empty($site['phone']))
                                    <li><i class="fa fa-phone"></i>  {{ $site['phone'] }}</li>
                                @endif
                            @endforelse
                            {{--<li><i class="fa fa-globe"></i> <a href="#">http://www.example.com</a></li>--}}

                            @if(!empty($site['social']))
                                @if(!empty($site['social']['twitter']))<li><a href="{{ $site['social']['twitter'] }}" target="_blank"><i class="fa fa-twitter"></i> {{ $site['social']['twitter'] }}</a></li> @endif
                                @if(!empty($site['social']['facebook']))<li><a href="{{ $site['social']['facebook'] }}" target="_blank"><i class="fa fa-facebook"></i> {{ $site['social']['facebook'] }}</a></li> @endif
                                @if(!empty($site['social']['gplus']))<li><a href="{{ $site['social']['gplus'] }}" target="_blank"><i class="fa fa-google-plus"></i> {{ $site['social']['gplus'] }}</a></li> @endif
                                @if(!empty($site['social']['youtube'])))<li><a href="{{ $site['social']['youtube'] }}" target="_blank"><i class="fa fa-youtube-play"></i> {{ $site['social']['youtube'] }}</a></li> @endif
                            @endif

                        </ul>
                    </div>
                    <div class="list_block">
                        <h3>Horário de Atendimento</h3>
                        <ul class="contact_info">
                            <li><strong>Segunda à Sexta:</strong> 08:00 à 20:00</li>
                            {{--<li><strong>Saturday:</strong> 11am to 3pm</li>--}}
                            {{--<li><strong>Sunday:</strong> Closed</li>--}}
                        </ul>
                    </div><!--end sidebar item-->
                    <div class="list_block" style="display: none">
                        <div class="newsletter">
                            <h3>Newsletter</h3>
                            <form method="post" action="#">
                                <div class="form-group">
                                    <input type="text" placeholder="Email" id="exampleInputEmail1" class="form-control">
                                </div>
                                <button class="btn btn-default btn-block commonBtn" type="submit">Subscribe</button>
                            </form>
                        </div>
                    </div>
                </div><!--end sidebar-->
            </div>
        </div><!--end row-->
    </div>
</div><!--end custom content-->

<div class="contact_map">

    <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d1837.5493095283782!2d-43.17157234186838!3d-22.909733075616757!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x9981ddf9464ef3%3A0x722f6cb722af256d!2sCentro%2C+Rio+de+Janeiro+-+RJ%2C+20021-120!5e0!3m2!1spt-BR!2sbr!4v1522356732985"
            width="100%" height="350" frameborder="0" style="border:0" allowfullscreen></iframe>
</div><!--end contact map-->

@include('footer')

