@include('header')


<div class="custom_content custom">
    <div class="container large">
        <div class="row">
            <div class="col-xs-12 col-sm-8 custom_right">
                <div class="single_content_left">
                    <h3>Entre em contato..</h3>
                    <p>{!! $message !!}</p>
                    <div class="contact_form">
                        <form method="post" action="http://crm.eaducam.com.br/leads/importacao.asp?">

                            @if(isset($produto) && !empty($produto['id_produto']))

                            <div class="row">
                                <div class="col-xs-12 col-sm-7">
                                    <div class="form-group">
                                        <label>Produto </label>
                                        <h4>{{ $produto['st_produto'] }}</h4>
                                        <input type="hidden" class="form-control" name="id_produto" value="{{ $produto['id_produto'] }}">
                                        <input type="hidden" class="form-control" name="nu_valorliquido" value="{{ $produto['nu_valorvenda'] }}">
                                    </div>
                                </div>
                            </div><!--end row-->

                            @endif

                            <div class="row">
                                <div class="col-xs-12 col-sm-7">
                                    <div class="form-group">
                                        <label>Nome </label>
                                        <input type="text" class="form-control" name="firstname">
                                    </div>
                                </div>
                            </div><!--end row-->
                            <div class="row">
                                <div class="col-xs-3 col-sm-2">
                                    <div class="form-group">
                                        <label>DDD </label>
                                        <input type="text" class="form-control" name="celddd" maxlength="3">
                                    </div>
                                </div>
                                <div class="col-xs-9 col-sm-5">
                                    <div class="form-group">
                                        <label>Telefone </label>
                                        <input type="number" class="form-control" name="celular" maxlength="9">
                                    </div>
                                </div>
                            </div><!--end row-->
                            <div class="row">
                                <div class="col-xs-12 col-sm-7">
                                    <div class="form-group">
                                        <label>E-mail </label>
                                        <input type="text" class="form-control" name="email">
                                    </div>
                                </div>
                            </div><!--end row-->
                            <div class="row">
                                <div class="col-xs-12 col-sm-11">
                                    <div class="form-group">
                                        <label>Mensagem </label>
                                        <textarea class="form-control" name="obs" cols="10" rows="9"></textarea>
                                    </div>
                                </div>
                            </div><!--end row-->
                            <input type="submit" value="Enviar Mensagem" class="commonBtn">
                            {{--<input type="hidden" name="_token" value="{{ csrf_token() }}">--}}

                            <input type="text" name="origem" value="ORIGEM:<?php echo $_SESSION['origem'] ?>" hidden>
					        <input type="text" name="abordagem" value="CONTATO-DO-SITE" hidden>
					        <input type="text" name="urls" value="http://eaducam.com.br/obrigado" hidden>
                        </form>
                    </div>
                </div><!--end single content left-->
            </div>

            <div class="col-xs-12 col-sm-4 custom_left">
                <div class="sidebar">
                    <div class="list_block sidebar_item">
                        <h3>Canais</h3>
                        <ul class="contact_info">
                            <li><i class="fa fa-home"></i>
                                {{ $site['address']['street'] }}, {{ $site['address']['number'] }}<br>
                                {{ $site['address']['extra'] }}, {{ $site['address']['district'] }}<br>
                                {{ $site['address']['city'] }}/{{ $site['address']['state'] }}<br>
                                {{ $site['address']['zipcode'] }}
                            </li>
                            <li><i class="fa fa-home"></i>
                                {{ $site['address2']['street'] }}, {{ $site['address2']['number'] }}<br>
                                {{ $site['address2']['extra'] }}, {{ $site['address2']['district'] }}<br>
                                {{ $site['address2']['city'] }}/{{ $site['address2']['state'] }}<br>
                                {{ $site['address2']['zipcode'] }}
                            </li>

                            @if(!empty($site['email']))
                                <li><i class="fa fa-envelope"></i> <a
                                            href="mailto:{{ $site['email']  }}">{{ $site['email']  }}</a></li>
                            @endif

                            @forelse ($site['phones'] as $phone)
                                <li><i class="fa fa-phone"></i> {{ $phone }}</li>
                            @empty
                                @if(!empty($site['phone']))
                                    <li><i class="fa fa-phone"></i> {{ $site['phone'] }}</li>
                                @endif
                            @endforelse
                            {{--<li><i class="fa fa-globe"></i> <a href="#">http://www.example.com</a></li>--}}

                            @if(!empty($site['social']))
                                @if(!empty($site['social']['twitter']))
                                    <li><a href="{{ $site['social']['twitter'] }}" target="_blank"><i
                                                    class="fa fa-twitter"></i> {{ $site['social']['twitter'] }}</a>
                                    </li> @endif
                                @if(!empty($site['social']['facebook']))
                                    <li><a href="{{ $site['social']['facebook'] }}" target="_blank"><i
                                                    class="fa fa-facebook"></i> {{ $site['social']['facebook'] }}</a>
                                    </li> @endif
                                @if(!empty($site['social']['gplus']))
                                    <li><a href="{{ $site['social']['gplus'] }}" target="_blank"><i
                                                    class="fa fa-google-plus"></i> {{ $site['social']['gplus'] }}</a>
                                    </li> @endif
                                @if(!empty($site['social']['youtube'])))
                                <li><a href="{{ $site['social']['youtube'] }}" target="_blank"><i
                                                class="fa fa-youtube-play"></i> {{ $site['social']['youtube'] }}</a>
                                </li> @endif
                            @endif

                        </ul>
                    </div>
                    <div class="list_block">
                        <h3>Horário de Atendimento</h3>
                        <ul class="contact_info">
                            <li><strong>Segunda à Sexta:</strong> 08:00 à 20:00</li>
                            <li><strong>Sábado:</strong>8:00 à 12:00</li>
                            {{--<li><strong>Sunday:</strong> Closed</li>--}}
                        </ul>
                    </div><!--end sidebar item-->
                    <div class="list_block" style="display: none">
                        <div class="newsletter">
                            <h3>Newsletter</h3>
                            <form method="post" action="#">
                                <div class="form-group">
                                    <input type="text" placeholder="Email" id="exampleInputEmail1" class="form-control">
                                </div>
                                <button class="btn btn-default btn-block commonBtn" type="submit">Subscribe</button>
                            </form>
                        </div>
                    </div>
                </div><!--end sidebar-->
            </div>
        </div><!--end row-->
    </div>
</div><!--end custom content-->

<div class="contact_map">

    <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d229.7028763080507!2d-43.17495613230443!3d-22.90429483132895!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x997f5f64a74be9%3A0x1ade0048ea8d4da7!2sR.+da+Assembl%C3%A9ia%2C+10+-+7%C2%BA+andar+-+Centro%2C+Rio+de+Janeiro+-+RJ%2C+20011-901!5e0!3m2!1spt-BR!2sbr!4v1522704845780"
            width="100%" height="350" frameborder="0" style="border:0" allowfullscreen></iframe>
</div><!--end contact map-->

@include('footer')

