@include('header')

<div class="single_banner">
    <div class="container">
        <div class="single_banner_inner" style="min-height: 200px; position: relative">
            {{--<img src="/img/courses/banner_01.jpg" alt=""/>--}}
            <div class="single_caption">
                <h1>{{ $curso['st_tituloexibicao'] }}</h1>
            </div><!--end single caption-->
        </div><!--end single_banner_inner-->
    </div><!--end container-->
</div><!--end single banner-->

<div class="single_content">
    <div class="container">
        <div class="row">

            <div class="col-xs-12 col-sm-8 col-md-8 custom_right">
                <div class="single_content_left padding-border-right-twenty">
                    <div class="tab_menu">
                        <ul>
                            <li class="active"><a href="#explore_program" data-toggle="tab">Conheça o Curso</a></li>
                        </ul>
                    </div><!--end tab_menu-->
                    <div class="tab-content single_tab_content">
                        <div role="tabpanel" class="tab-pane active" id="explore_program">

                            @if(!empty(trim(strip_tags($produto['st_descricao']))))
                                <h1>{{ $produto['st_produto'] }}</h1>
                                <p>{!! $produto['st_descricao'] !!}</p>
                            @endif

                            @if(!empty(trim(strip_tags($produto['st_objetivo']))))
                                <h1>Objetivo</h1>
                                <p>{!! $produto['st_objetivo'] !!}</p>
                            @endif

                            @if(!empty($curso['Areas']))
                                <h1>Áreas</h1>
                                <p>
                                <ul>
                                    @foreach($curso['Areas'] as $area)
                                        <li>{!! $area['st_tituloexibicao'] !!}</li>
                                    @endforeach
                                </ul>
                                </p>
                            @endif

                            @if(!empty(trim(strip_tags($curso['st_publicoalvo']))))
                                <h1>Público Alvo</h1>
                                <p>{!! $curso['st_publicoalvo'] !!}</p>
                            @endif

                            @if(!empty($curso['st_metodologia']))
                                <h1>Metodologia</h1>
                                <p>{!! $curso['st_metodologia'] !!}</p>
                            @endif

                            @if(!empty($curso['Disciplinas']))
                                <h1>Disciplinas</h1>
                                <table class="table table-bordered table-striped">
                                    <thead>
                                    <tr>
                                        <th style="background:#800000; color:#fff;">Disciplina</th>
                                        <th style="background:#800000; color:#fff;">Carga Horária</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($curso['Disciplinas'] as $disciplina)
                                        @if ($disciplina['st_disciplina'] != 'Ambientação.')
                                            <tr>
                                                <td>{{ $disciplina['st_disciplina'] }}</td>
                                                <td>{{ $disciplina['nu_cargahoraria'] }} Horas</td>
                                            </tr>
                                        @endif
                                    @endforeach
                                    </tbody>
                                </table>
                            @endif
                            <!-- FIM TABELA DISCIPLINAS -->

                            @if(!empty($curso['st_certificacao']))
                                <h1>Certificação</h1>
                                <p>{!! $curso['st_certificacao'] !!}</p>
                            @endif

                            @if(!empty($produto['st_observacoes']))
                                <h1>Investimento</h1>
                                <p>{!! $produto['st_observacoes'] !!}</p>
                            @endif

                            @if(!empty($produto['st_duracao']))
                                <h1>Duração</h1>
                                <p>{!! $produto['st_duracao'] !!}</p>
                            @endif

                            @if(!empty($curso['nu_cargahoraria']))
                                <h1>Carga Horária</h1>
                                <p>{!! $curso['nu_cargahoraria'] !!}</p>
                            @endif



                            {{--@if(!empty(trim(strip_tags($curso['st_perfilprojeto']))))--}}
                            {{--<h1>Perfil do Projeto</h1>--}}
                            {{--<p>{!! $curso['st_perfilprojeto'] !!}</p>--}}
                            {{--@endif--}}

                            {{--@if(!empty(trim(strip_tags($curso['st_mercadotrabalho']))))--}}
                            {{--<h1>Mercado de Trabalho</h1>--}}
                            {{--<p>{!! $curso['st_mercadotrabalho'] !!}</p>--}}
                            {{--@endif--}}


                            {{--@if(!empty(trim(strip_tags($curso['st_coordenacao']))))--}}
                            {{--<h1>Coordenação</h1>--}}
                            {{--<p>{!! $curso['st_coordenacao'] !!}</p>--}}
                            {{--@endif--}}


                        </div><!--end tab-pane-->
                    </div><!--end tab-content-->
                </div><!--end single content left-->
            </div><!--end custom_right-->

            <div class="col-xs-12 col-sm-4 col-md-4 custom_left">
                <div class="sidebar">
                    

                    <!-- xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx -->
                    
                    <div class="sidebar_item">
                        <div class="item_inner program">
                            
                                <h2 class="titulo-form">PREENCHA OS CAMPOS ABAIXO PARA MAIS INFORMAÇÕES.</h2>
                                <form name="classic" action="http://crm.eaducam.com.br/leads/importacao.asp?" method="post" class=""> <!-- http://crm.eaducam.com.br/leads/importacao.asp? confirmacao.php-->
                                
                                    <div class="fields col-esquerda col-12 col-md-6">
                                        <label>ESCOLHA A ÁREA<span class="text-vinho">*</span></label><br>
                                        <select id="categoria" onchange="OnSelectionChange()" class="select-curso" name="tipocurso" required="">
                                            <option selected=""></option>
                                            @foreach($categorias as $categoria)
                                                <option value="{{ $categoria['st_categoria'] }}">{{ $categoria['st_categoria'] }}</option>
                                            @endforeach
                                        </select>
                                    </div>                                    

                                    <div class="fields col-md-6">
                                        <label>ESCOLHA SEU CURSO<span class="text-vinho">*</span></label><br>
                                        <select id="direito" class="select-curso" name="curso">
                                            <option selected=""></option>                           
                                            @foreach ($categorias as $item) {
                                                @foreach ($item['produtos'] as $item2) {
                                                    @if ($item2['id_categoria'] == 101) {
                                                        <option value="{{ $item2['st_produto'] }}">{{ $item2['st_produto'] }}</option>
                                                    @endif
                                                @endforeach
                                            @endforeach                                       
                                        </select>

                                        <select id="educacao" class="select-curso" name="curso" hidden>
                                            <option selected=""></option>
                                            @foreach ($categorias as $item) {
                                                @foreach ($item['produtos'] as $item2) {
                                                    @if ($item2['id_categoria'] == 96) {
                                                        <option value="{{ $item2['st_produto'] }}">{{ $item2['st_produto'] }}</option>
                                                    @endif
                                                @endforeach
                                            @endforeach
                                        </select>

                                        <select id="gestao-publica" class="select-curso" name="curso" hidden>
                                            <option selected=""></option>
                                            @foreach ($categorias as $item) {
                                                @foreach ($item['produtos'] as $item2) {
                                                    @if ($item2['id_categoria'] == 94) {
                                                        <option value="{{ $item2['st_produto'] }}">{{ $item2['st_produto'] }}</option>
                                                    @endif
                                                @endforeach
                                            @endforeach
                                        </select>

                                        <select id="mba" class="select-curso" name="curso" hidden>
                                            <option selected=""></option>
                                            @foreach ($categorias as $item) {
                                                @foreach ($item['produtos'] as $item2) {
                                                    @if ($item2['id_categoria'] == 100) {
                                                        <option value="{{ $item2['st_produto'] }}">{{ $item2['st_produto'] }}</option>
                                                    @endif
                                                @endforeach
                                            @endforeach
                                        </select>

                                        <select id="negocios" class="select-curso" name="curso" hidden>
                                            <option selected=""></option>
                                            @foreach ($categorias as $item) {
                                                @foreach ($item['produtos'] as $item2) {
                                                    @if ($item2['id_categoria'] == 98) {
                                                        <option value="{{ $item2['st_produto'] }}">{{ $item2['st_produto'] }}</option>
                                                    @endif
                                                @endforeach
                                            @endforeach
                                        </select>

                                        <select id="saude" class="select-curso" name="curso" hidden>
                                            <option selected=""></option>
                                            @foreach ($categorias as $item) {
                                                @foreach ($item['produtos'] as $item2) {
                                                    @if ($item2['id_categoria'] == 103) {
                                                        <option value="{{ $item2['st_produto'] }}">{{ $item2['st_produto'] }}</option>
                                                    @endif
                                                @endforeach
                                            @endforeach
                                        </select>

                                        <select id="seguranca" class="select-curso" name="curso" hidden>
                                            <option selected=""></option>
                                            @foreach ($categorias as $item) {
                                                @foreach ($item['produtos'] as $item2) {
                                                    @if ($item2['id_categoria'] == 97) {
                                                        <option value="{{ $item2['st_produto'] }}">{{ $item2['st_produto'] }}</option>
                                                    @endif
                                                @endforeach
                                            @endforeach
                                        </select>
                                    </div>
                                   

                                    <div class=" fields col-esquerda col-md-6">
                                        <label>NOME<span class="text-vinho">*</span></label><br>
                                            <input name="firstname" type="text" class="field" required="" placeholder="">
                                    </div>

                                    <div class="fields col-md-6">
                                        <label>SOBRENOME<span class="text-vinho">*</span></label><br>
                                            <input name="lastname" type="text" class="field" required="" placeholder="">
                                    </div>

                                    <div class="fields col-esquerda col-md-6">
                                        <label>E-MAIL<span class="text-vinho">*</span></label><br>
                                            <input name="email" type="email" class="field" required="" placeholder="">
                                    </div>

                                    <div class="fields col-md-6">
                                        <label>CELULAR<span class="text-vinho">*</span></label><br>
                                            <input name="celular" id="phone-cel" type="text" class="field" required="" placeholder="(00) 00000-0000">
                                    </div>

                                    <div class="fields col-esquerda col-md-6">
                                        <label>TELEFONE</label><br>
                                        <input name="telefone" id="phone" type="text" class="field" placeholder="(00) 0000-0000" maxlength="14">
                                    </div>

                                    <div class="fields col-md-6">
                                        <label>UF<span class="text-vinho">*</span></label><br>
                                            <select id="uf" name="flag1">
                                                <option selected=""></option>
                                                <option value="AC">AC</option>
                                                <option value="AL">AL</option>
                                                <option value="AP">AP</option>
                                                <option value="AM">AM</option>
                                                <option value="BA">BA</option>
                                                <option value="CE">CE</option>
                                                <option value="DF">DF</option>
                                                <option value="ES">ES</option>
                                                <option value="GO">GO</option>
                                                <option value="MA">MA</option>
                                                <option value="MT">MT</option>
                                                <option value="MS">MS</option>
                                                <option value="MG">MG</option>
                                                <option value="PA">PA</option>
                                                <option value="PB">PB</option>
                                                <option value="PR">PR</option>
                                                <option value="PE">PE</option>
                                                <option value="PI">PI</option>
                                                <option value="RJ">RJ</option>
                                                <option value="RN">RN</option>
                                                <option value="RS">RS</option>
                                                <option value="RO">RO</option>
                                                <option value="PR">RR</option>
                                                <option value="SC">SC</option>
                                                <option value="SP">SP</option>
                                                <option value="SE">SE</option>
                                                <option value="TO">TO</option>
                                            </select>
                                    </div>
                                                
                                    <input type="text" name="origem" value="ORIGEM:<?php echo $_SESSION['origem'] ?> " hidden="">
                                    <input type="text" name="abordagem" value="INTERESSE" hidden="">
                                    <input type="text" name="urls" value="http://eaducam.com.br/obrigado" hidden="">
                            
                                    <div class="botao-enviar-div">
                                        <input type="submit" name="enviar" value="ENVIAR" class="botao-enviar"><br>
                                        <p><span class="text-vinho">*</span>Campos com prenchimento obrigatório</p>

                                    </div>
                                </form>
                                                      
                        </div>
                    </div>

                    <!-- xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx -->

                    <div class="sidebar_item">
                        <div class="item_inner program">
                            <h4>Detalhes</h4>
                            <ul>
                                {{--<li><span>Categoria:</span>{{ $curso['st_categoria'] }}</li>--}}
                                <li><span>Carga Horária:</span> {{ $curso['nu_cargahoraria'] }}</li>
                                <li><span>A partir de:</span> R$  {{ number_format($produto['nu_valormensal'], 2, ',', '.') }} (mensais)</li>
                                <li style="font-size: 10px"><i class="fa fa-info-circle"></i> Valores promocionais. Para mais informações por favor entre em contato por algum dos números abaixo.</li>
                                {{--<li><span>Study Options:</span>Full Time</li>--}}
                                {{--<li><span>Campus Location:</span>College Drive</li>--}}
                            </ul>
                        </div>
                    </div>


                    <!-- xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx -->


                    {{--<div class="sidebar_item" style="padding: 0px; border-top: 0px">
                        <a class="btn btn-info btn-matricule-se btn-block" href="/contato/{{ $produto['id_produto'] }}/mais-informações" role="button">Entre em contato</a>
                    </div><!--end sidebar item-->--}}
                    <div class="sidebar_item" style="padding: 0px; border-top: 0px">
                        <a class="btn btn-success btn-matricule-se btn-block" style="margin-top: 10px" href="/curso/{{ $curso['id_projetopedagogico'] }}/matricular" role="button">Matricule-se</a>
                    </div><!--end sidebar item-->
                    <!--end sidebar item-->
                    {{--<div class="sidebar_item">--}}
                        {{--<div class="item_inner">--}}
                            {{--<h4>Tution Fees</h4>--}}
                            {{--<a href="#" class="fees">View 2015-2016 Tution Fees</a>--}}
                        {{--</div>--}}
                    {{--</div><!--end sidebar item-->--}}
                    {{--<div class="sidebar_item">--}}
                        {{--<div class="item_inner slider">--}}
                            {{--<h4>What Students Say</h4>--}}
                            {{--<div id="single_banner">--}}
                                {{--<ul class="slides">--}}
                                    {{--<li>--}}
                                        {{--<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod--}}
                                            {{--tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,--}}
                                            {{--quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo--}}
                                            {{--consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse--}}
                                            {{--cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat--}}
                                            {{--non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.--}}
                                            {{--Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium--}}
                                            {{--doloremque laudantium,</p>--}}
                                        {{--<div class="carousal_bottom">--}}
                                            {{--<div class="thumb">--}}
                                                {{--<img src="img/courses/testi_thumb.png" draggable="false" alt=""/>--}}
                                            {{--</div>--}}
                                            {{--<div class="thumb_title">--}}
                                                {{--<span class="author_name">Jeremy Asigner</span>--}}
                                                {{--<span class="author_designation">Web Developer<a--}}
                                                            {{--href="#"> Here</a></span>--}}
                                            {{--</div>--}}
                                        {{--</div>--}}
                                    {{--</li>--}}
                                    {{--<li style="display:none;">--}}
                                        {{--<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod--}}
                                            {{--tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,--}}
                                            {{--quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo--}}
                                            {{--consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse--}}
                                            {{--cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat--}}
                                            {{--non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.--}}
                                            {{--Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium--}}
                                            {{--doloremque laudantium,</p>--}}
                                        {{--<div class="carousal_bottom">--}}
                                            {{--<div class="thumb">--}}
                                                {{--<img src="img/courses/testi_thumb.png" draggable="false" alt=""/>--}}
                                            {{--</div>--}}
                                            {{--<div class="thumb_title">--}}
                                                {{--<span class="author_name">Jeremy Asigner</span>--}}
                                                {{--<span class="author_designation">Web Developer<a--}}
                                                            {{--href="#">Here</a></span>--}}
                                            {{--</div>--}}
                                        {{--</div>--}}
                                    {{--</li>--}}
                                    {{--<li style="display:none;">--}}
                                        {{--<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod--}}
                                            {{--tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,--}}
                                            {{--quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo--}}
                                            {{--consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse--}}
                                            {{--cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat--}}
                                            {{--non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.--}}
                                            {{--Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium--}}
                                            {{--doloremque laudantium,</p>--}}
                                        {{--<div class="carousal_bottom">--}}
                                            {{--<div class="thumb">--}}
                                                {{--<img src="img/courses/testi_thumb.png" draggable="false" alt=""/>--}}
                                            {{--</div>--}}
                                            {{--<div class="thumb_title">--}}
                                                {{--<span class="author_name">Jeremy Asigner</span>--}}
                                                {{--<span class="author_designation">Web Developer<a--}}
                                                            {{--href="#">Here</a></span>--}}
                                            {{--</div>--}}
                                        {{--</div>--}}
                                    {{--</li>--}}
                                {{--</ul>--}}
                            {{--</div><!--end banner-->--}}
                        {{--</div><!--slider-->--}}
                    {{--</div><!--end sidebar item-->--}}

                    <div class="sidebar_item">
                        <div class="item_inner question">
                            <h4>Alguma dúvida?</h4>
                            <h5>Entre em contato:</h5>
                            @forelse ($site['phones'] as $phone)
                                <h4 class="phoneNo"><i class="fa fa-phone"></i> {{ $phone }}</h4>
                            @empty
                                @if(!empty($site['phone'])) <h4 class="phoneNo"><i class="fa fa-phone"></i> {{ $site['phone'] }}</h4> @endif
                            @endforelse
                        </div>
                    </div><!--end sidebar item-->


                </div><!--end sidebar-->
            </div>

        </div><!--end row-->
    </div><!--end container-->
</div><!--end single content-->

@include('footer')