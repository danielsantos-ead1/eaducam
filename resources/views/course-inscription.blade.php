@include('header')

<div class="single_content">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="page-header" style="text-align: center">
                    <h1>{{ $curso['st_tituloexibicao'] }}</h1>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12">
                <div class="page-header">
                    <h2>Estamos quase lá! <br><small>Verifique abaixo e clique em Proseguir para finalizar o processo</small></h2>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12">
                <div class="cartListInner">

                        <div class="table-responsive">
                            <table class="table">
                                <thead>
                                <tr>
                                    <th>Curso</th>
                                    <th style="text-align: right; padding: 10px">Valor</th>
                                </tr>
                                </thead>
                                <tbody>
                                <tr>
                                    <td class="col-md-6 col-xs-5">
                                        <strong>{{ $curso['st_tituloexibicao'] }}</strong>
                                    </td>
                                    <td class="col-md-3 col-xs-2">
                                        Mensais de R$  {{ number_format($curso['nu_valormensal'], 2, ',', '.') }}
                                    </td>
                                </tr>

                                {{--<tr>--}}
                                    {{--<td class="col-md-6 col-xs-5">--}}
                                        {{--<strong>Total</strong>--}}
                                    {{--</td>--}}
                                    {{--<td class="col-md-3 col-xs-2">--}}
                                        {{--<span class="cart-price">R$ {{ number_format($curso['nu_valormensal'], 2, ',', '.') }}</span>--}}
                                    {{--</td>--}}
                                {{--</tr>--}}

                                <tr>
                                    <td colspan="2" style="text-align: center">
                                        <div class="alert alert-info" role="alert" style="font-size: 16px">
                                            Ao clicar abaixo em Prosseguir para o Pagamento, você será redirecionado ao nosso ambiente seguro para continuar sua compra.
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2">

                                        <form action="{{ $site['checkout_url'] }}" method="post" target="_parent">
                                            <input type="hidden" name="produtos[0][id_produto]" value="{{ $curso['id_produto'] }}" />
                                            <input type="hidden" name="produtos[0][nu_quantidade]" value="1" />
                                            <input type="hidden" name="st_codchave" value="{{ $site['st_chave'] }}">
                                            <input type="hidden" name="st_urlconfirmacao" value="http://eaducam.com.br/">
                                            <button type="submit" class="btn btn-success btn-block">Prosseguir para o Pagamento</button>
                                        </form>

                                    </td>
                                </tr>
                                </tbody>
                            </table>
                        </div>

                </div>

            </div>
        </div>


    </div><!--end container-->
</div><!--end single content-->

@include('footer')