<a href="https://api.whatsapp.com/send?phone=5561986159090&amp;text=Ol%C3%A1%21%20%20gostaria%20de%20saber%20mais%20sobre%20os%20cursos%20da%20UCAM." class="whatsapp-btn tooltips" data-placement="right" title="" target="blank" data-original-title="Contate o Suporte">
  <i class="fa fa-whatsapp fa-3x"></i>
</a>

<footer class="footer-v1">
  <div class="menuFooter clearfix">
    <div class="container">
      <div class="row clearfix">

        {{--<div class="col-sm-3 col-xs-6">--}}
          {{--<ul class="menuLink">--}}
            {{--<li><a href="about.html">About Royal College</a></li>--}}
            {{--<li><a href="campus.html">About Campus</a></li>--}}
            {{--<li><a href="stuff.html">Staff Members</a></li>--}}
            {{--<li><a href="about.html">Why Choose Us?</a></li>--}}
          {{--</ul>--}}
        {{--</div><!-- col-sm-3 col-xs-6 -->--}}

        <div class="col-sm-4 col-xs-12">
          <ul class="menuLink">
            <li><a href="/" target="_self">Início</a></li>
            <li><a href="/cursos" target="_self">Cursos</a></li>
            <li><a href="/inova-ead" target="_self" style="color: red;">iNOVA-ead</a></li>
            <li><a href="/entre-em-contato" target="_self">Contato</a></li>
            <li class="apply_now"><a href="/cursos">Matricule-se!</a></li>
          </ul>
        </div><!-- col-sm-3 col-xs-6 -->

        <div class="col-sm-4 col-xs-6 borderLeft">
          <div class="footer-address">
            <h5>Endereços:</h5>
            <address>
              {{ $site['address']['street'] }}, {{ $site['address']['number'] }}<br>
              {{ $site['address']['extra'] }}, {{ $site['address']['district'] }}<br>
              {{ $site['address']['city'] }}/{{ $site['address']['state'] }}<br>
              {{ $site['address']['zipcode'] }}
            </address>
            <hr size="1">
            <address>
              {{ $site['address2']['street'] }}, {{ $site['address2']['number'] }}<br>
              {{ $site['address2']['extra'] }}, {{ $site['address2']['district'] }}<br>
              {{ $site['address2']['city'] }}/{{ $site['address2']['state'] }}<br>
              {{ $site['address2']['zipcode'] }}
            </address>
          </div>
        </div><!-- col-sm-3 col-xs-6 -->

        <div class="col-sm-4 col-xs-6 borderLeft">
          <div class="socialArea">
            <h5>Entre em contato:</h5>
            <ul class="list-inline ">
            @if(!empty($site['social']))
              @if(!empty($site['social']['twitter']))<li><a href="{{ $site['social']['twitter'] }}" target="_blank"><i class="fa fa-twitter"></i></a></li> @endif
              @if(!empty($site['social']['facebook']))<li><a href="{{ $site['social']['facebook'] }}" target="_blank"><i class="fa fa-facebook"></i></a></li> @endif
              @if(!empty($site['social']['gplus']))<li><a href="{{ $site['social']['gplus'] }}" target="_blank"><i class="fa fa-google-plus"></i></a></li> @endif
              @if(!empty($site['social']['youtube'])))<li><a href="{{ $site['social']['youtube'] }}" target="_blank"><i class="fa fa-youtube-play"></i></a></li> @endif
            @endif
            </ul>
          </div><!-- social -->
          
          
          
          <div class="contactNo">
            
            @forelse ($site['phones'] as $phone)
              <h4 class="phoneNo"><i class="fa fa-phone"></i> {{ $phone }}</h4>
            @empty
              @if(!empty($site['phone'])) <h4 class="phoneNo"><i class="fa fa-phone"></i> {{ $site['phone'] }}</h4> @endif
            @endforelse
              <h4 class="phoneNo"><i class="fa fa-whatsapp"></i> {{ $site['whats'] }}</h4>
            
          </div><!-- contactNo -->

          <div>
            <img style="margin-top: 30px;" src="/img/logo-ucam-inova-ead.png" alt="">
          </div>
        </div><!-- col-sm-3 col-xs-6 -->

      </div><!-- row -->
    </div><!-- container -->
  </div><!-- menuFooter -->

  <div class="footer clearfix">
    <div class="container">
      {{--<div class="row clearfix">--}}
        {{--<div class="col-sm-6 col-xs-12 copyRight">--}}
          {{--<p>© 2016 Copyright Royal College Bootstrap Template by <a href="http://www.iamabdus.com">Abdus</a></p>--}}
        {{--</div><!-- col-sm-6 col-xs-12 -->--}}
        {{--<div class="col-sm-6 col-xs-12 privacy_policy">--}}
          {{--<a href="contact-us.html">Contact us</a>--}}
          {{--<a href="privacy-policy.html">Privacy Policy</a>--}}
        {{--</div><!-- col-sm-6 col-xs-12 -->--}}
      {{--</div><!-- row clearfix -->--}}
    </div><!-- container -->
  </div><!-- footer -->
</footer>

</div>

<!-- JQUERY SCRIPTS -->
<script src="/plugins/jquery/jquery-1.11.1.min.js"></script>
<script src="/plugins/bootstrap/js/bootstrap.min.js"></script>
<script src="/plugins/flexslider/jquery.flexslider.js"></script>
<script src="/plugins/rs-plugin/js/jquery.themepunch.tools.min.js"></script>
<script src="/plugins/rs-plugin/js/jquery.themepunch.revolution.min.js"></script>
<script src="/plugins/selectbox/jquery.selectbox-0.1.3.min.js"></script>
<script src="/plugins/pop-up/jquery.magnific-popup.js"></script>
<script src="/plugins/animation/waypoints.min.js"></script>
<script src="/plugins/count-up/jquery.counterup.js"></script>
<script src="/plugins/animation/wow.min.js"></script>
<script src="/plugins/animation/moment.min.js"></script>
<script src="/plugins/calender/fullcalendar.min.js"></script>
<script src="/plugins/owl-carousel/owl.carousel.js"></script>
<script src="/plugins/timer/jquery.syotimer.js"></script>
<script src="/plugins/smoothscroll/SmoothScroll.js"></script>
<script src="/js/custom.js"></script>

  
</body>
</html>

