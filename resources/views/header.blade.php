<?php 
  session_start();
  if (isset($_REQUEST['parceiro'])){
      $_SESSION['origem'] = $_REQUEST['parceiro'];
  } else if (isset($_REQUEST['convenio'])) {
      $_SESSION['origem'] = $_REQUEST['convenio'];
  } else if ($_REQUEST == NULL){
      $_SESSION['origem'] = 'ACESSO_DIRETO';
  }
?>

<!DOCTYPE html>
<html lang="en">
<head>
  <!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-119262872-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-119262872-1');
</script>
  <!-- END Global site tag (gtag.js) - Google Analytics -->

<!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-53CPBSV');</script>
<!-- End Google Tag Manager -->

  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="keywords" content="pos graduação, e pos graduação, pos graduação em, cursos de pos graduação, curso pos graduação, pos graduação cursos, pos graduacoes, a pós graduação, pos graduação a distancia sp, cursos de pos, pos graduação a distancia, pos graduação distancia, pos graduação em ead, pos graduação a distancia, curso pos graduação a distancia, pos graduação a distancia ead, cursos de pos graduação a distancia, pos em ead, ead pos graduação, pos ead, Pós segurança, Pós educação, Pós gestão pública, Pós saúde, Pós enfermagem, Pós MBA, Gerência de projetos, Economia e finanças, Big data"/>

  <title>{{ $site['name'] }}</title>

  <!-- PLUGINS CSS STYLE -->
  <link rel="icon" type="image/png" href="img/favicon.png">
  <link href="/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet">
  <link rel="stylesheet" href="/plugins/selectbox/select_option1.css">
  <link rel="stylesheet" href="/plugins/font-awesome/css/font-awesome.min.css">
  <link rel="stylesheet" href="/plugins/flexslider/flexslider.css" type="text/css" media="screen" />
  <link rel="stylesheet" href="/plugins/calender/fullcalendar.min.css">
  <link rel="stylesheet" href="/plugins/animate.css">
  <link rel="stylesheet" href="/plugins/pop-up/magnific-popup.css">
  <link rel="stylesheet" type="text/css" href="/plugins/rs-plugin/css/settings.css" media="screen">
  <link rel="stylesheet" type="text/css" href="/plugins/owl-carousel/owl.carousel.css" media="screen">

  <!-- GOOGLE FONT -->
  <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,600,600italic,400italic,700' rel='stylesheet' type='text/css'>
  <link href='https://fonts.googleapis.com/css?family=Roboto+Slab:400,700' rel='stylesheet' type='text/css'>

  <!-- FONT AWESOME -->
  <!--<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.13/css/all.css" integrity="sha384-DNOHZ68U8hZfKXOrtjWvjxusGo9WQnrNx2sqG0tfsghAvtVlRW3tvkXWZh58N9jp" crossorigin="anonymous">-->

  <!-- CUSTOM CSS -->
  <link rel="stylesheet" href="/css/style.css">
  <link rel="stylesheet" href="/css/responsivo.css">
  <link rel="stylesheet" href="/css/ucam.css" id="option_color">

  <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->
</head>
<body class="body-wrapper">

<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-53CPBSV"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->

<!-- LOGIN MODAL -->
<div class="modal fade customModal" id="loginModal" tabindex="-1" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="panel panel-default formPanel">
        <div class="panel-heading">
          <h3 class="panel-title">Acessar o Portal do Aluno</h3>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        </div>

        <div class="panel-body">
          <form action="http://portal.ead1.net/login/autenticacao-externa" target="_blank" method="POST" role="form">
          {{--<form action="http://portaldoaluno.emgsoft.com.br/login/autenticacao-externa" target="_blank" method="POST" role="form">--}}
            <div class="form-group formField">
              <i class="fa fa-user" aria-hidden="true"></i>
              <input type="text" name="nomusuario" class="form-control" placeholder="Usuário">
            </div>
            <div class="form-group formField">
              <i class="fa fa-lock" aria-hidden="true"></i>
              <input type="password" name="dessenha" class="form-control" placeholder="Senha">
            </div>
            <button type="submit" class="btn btn-block commonBtn">Acessar</button>
            <div class="form-group" style="text-align: left">
              <br>
              <p>Caso tenha esquecido sua senha de acesso, você pode acessar <a href="http://portal.ead1.net/index" target="_blank">http://portal.ead1.net/</a> e clicar em Recuperar Senha</p>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
</div>

  <div class="main_wrapper">

    <header class="header-wrapper">
      <div class="topbar clearfix">
        <div class="container">
          <ul class="topbar-left">
            @forelse ($site['phones'] as $phone)
              <li class="phoneNo"><i class="fa fa-phone"></i> {{ $phone }}</li>
            @empty
              @if(!empty($site['phone'])) <li class="phoneNo"><i class="fa fa-phone"></i> {{ $site['phone'] }}</li> @endif
            @endforelse

            <li class="phoneNo"><i class="fa fa-whatsapp"></i> {{ $site['whats'] }}</li>

            @if(!empty($site['email']))
            <li class="email-id hidden-xs hidden-sm">
              <i class="fa fa-envelope"></i>
              <a href="mailto:{{ $site['email']  }}">{{ $site['email']  }}</a>
            </li>
            @endif

            <li>
              <i class="fa fa-lock"></i>
              <a href='#loginModal' data-toggle="modal" >Acessar o Portal do Aluno</a>
            </li>

          </ul>
          <ul class="topbar-right">

            @if(!empty($site['social']))
              @if(!empty($site['social']['twitter']))<li class="hidden-xs"><a href="{{ $site['social']['twitter'] }}" target="_blank"><i class="fa fa-twitter"></i></a></li> @endif
              @if(!empty($site['social']['facebook']))<li class="hidden-xs"><a href="{{ $site['social']['facebook'] }}" target="_blank"><i class="fa fa-facebook"></i></a></li> @endif
              @if(!empty($site['social']['gplus']))<li class="hidden-xs"><a href="{{ $site['social']['gplus'] }}" target="_blank"><i class="fa fa-google-plus"></i></a></li> @endif
              @if(!empty($site['social']['youtube'])))<li class="hidden-xs"><a href="{{ $site['social']['youtube'] }}" target="_blank"><i class="fa fa-youtube-play"></i></a></li> @endif
            @endif

            {{--<li class="hidden-xs"><a href="#"><i class="fa fa-rss"></i></a></li>--}}
            <li class="top-search list-inline">
              <a href="#"><i class="fa fa-search"></i></a>
              <ul class="dropdown-menu dropdown-menu-right">
                <li>
                  <span class="input-group">
                    <form name="top-search" method="get" action="/cursos">
                      <input type="text" name="st_texto" class="form-control" placeholder="Curso">
                      <button type="submit" class="btn btn-default commonBtn">Pesquisar</button>
                    </form>
                  </span>
                </li>
              </ul>
            </li>

          </ul>
        </div>
      </div>

      <div class="header clearfix">
        <nav class="navbar navbar-main navbar-default">
          <div class="container">
            <div class="row">
              <div class="col-xs-12">
                <div class="header_inner" style="min-height: 80px; padding-top: 10px">

                  <!-- Brand and toggle get grouped for better mobile display -->
                  <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#main-nav" aria-expanded="false">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    </button>
                    <a style="height: auto;" class="navbar-brand clearfix" href="/"><img src="/img/{{ $site['logo']  }}" alt="" class="img-responsive" /></a>
                  </div>

                  <!-- Collect the nav links, forms, and other content for toggling -->
                  <div class="collapse navbar-collapse" id="main-nav">
                    <ul class="nav navbar-nav navbar-right">
                      <li><a href="/" target="_self">Início</a></li>
                      <li><a href="/cursos" target="_self">Cursos</a></li>
                      <li><a href="/inova-ead" target="_self" style="color: red;">iNOVA-ead</a></li>
                      <li><a href="/alta-performance" target="_self">Alta Performance</a></li>
                      {{--<li><a href="/entre-em-contato" target="_self">Contato</a></li>--}}
                      <li><a href="http://blog.eaducam.com.br/" target="_blank">Blog</a></li>
                      <li class="apply_now"><a href="/contato">Entre em Contato</a></li>
                    </ul>
                  </div><!-- navbar-collapse -->

                </div>
              </div>
            </div>
          </div><!-- /.container -->
        </nav><!-- navbar -->
      </div>
    </header>
