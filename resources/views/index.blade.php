@include('header')

    @if(!empty($slider))
      <div class="container">
    <div class="banner carousel slide" id="recommended-item-carousel" data-ride="carousel">
      <div class="slides carousel-inner">
        @foreach($slider['items'] as $item)

        <div class="item @if ($loop->first) active @endif">

          <a href="{{ $item['url'] }}"><img src="{{ $item['image'] }}" alt="" /></a>
          <div class="banner_caption">
            <div class="container">
              <div class="row">
                <div class="col-xs-12">
                  <!--<div class="caption_inner animated fadeInUp">
                    <h1>{{ $item['title'] }}</h1>
                    <p>{{ $item['description'] }}</p>
                    <a href="{{ $item['url'] }}">{{ $item['url_text'] }}</a>
                  </div><!--end caption_inner-->
                </div>
              </div><!--end row-->
            </div><!--end container-->
          </div><!--end banner_caption-->
        </div>
        @endforeach
      </div>
      <a class="left recommended-item-control" href="#recommended-item-carousel" data-slide="prev">
        <img src="img/home/slider/prev.png">
        </a>
      <a class="right recommended-item-control" href="#recommended-item-carousel" data-slide="next">
        <img src="img/home/slider//next.png">
      </a>
    </div><!--end banner--></div>
    @endif

  <div class="banner-bottom">
    <form action="/cursos" method="get">
    <div class="container">
      <div class="banner-bottom-inner" style="position: static; z-index: auto; float: none; margin-top: 10px;">
        <div class="row">
          <div class="col-sm-4">
            <div class="selectBox select-category clearfix">
              <select name="id_categoria">
                <option value="0"><i class="fa fa-align-justify" aria-hidden="true"></i>Todas Categorias</option>
                @foreach($categorias as $categoria)
                <option value="{{ $categoria['id_categoria'] }}">{{ $categoria['st_categoria'] }}</option>
                @endforeach
              </select>
            </div>
          </div>
          <div class="col-sm-8">
            <div class="searchCourse">

                <input type="text" placeholder="Buscar pelo meu curso..." name="st_texto" class="form-control">
                <button class="btn btn-default commonBtn" type="submit">Buscar</button>

            </div>
          </div>

        </div>
      </div>
    </div>
    </form>
  </div>


    <div class="mainContent clearfix">
      <div class="container">

        @if(!empty($categorias))
        <div class="course-grid">
          <div class="about_inner clearfix">
            <div class="row">
              @foreach($categorias as $categoria)
              <div class="col-xs-6 col-sm-3">
                <div class="aboutImage">
                  <a href="/categoria/{{ $categoria['id_categoria'] }}/{{ mb_strtolower($categoria['st_nomeexibicao'], 'UTF-8') }}">
                    @if($categoria['st_imagemcategoria'])
                      <img src="{{ $categoria['st_imagemcategoria'] }}" alt="" class="img-responsive" />
                      @else
                      <img src="img/home/learn/learn_1.jpg" alt="" class="img-responsive" />
                    @endif
                    <div class="overlay">
                      <p>{{ $categoria['st_categoria'] }}</p>
                      <p>{{ count($categoria['produtos']) }} Cursos</p>
                    </div>
                    <span class="captionLink">{{ count($categoria['produtos']) }} Cursos, matricule-se!<span></span></span>
                  </a>
                </div><!-- aboutImage -->
                <h3>
                  <a href="/categoria/{{ $categoria['id_categoria'] }}/{{ mb_strtolower($categoria['st_nomeexibicao'], 'UTF-8') }}">{{ $categoria['st_nomeexibicao'] }} </a>
                </h3>
              </div>
              @endforeach
            </div><!-- row -->

            {{--<ul class="pagination">--}}
              {{--<li>--}}
                {{--<a aria-label="Previous" href="#">--}}
                  {{--<span aria-hidden="true">Previous</span>--}}
                {{--</a>--}}
              {{--</li>--}}
              {{--<li class="active"><a href="#">1</a></li>--}}
              {{--<li><a href="#">2</a></li>--}}
              {{--<li><a href="#">3</a></li>--}}
              {{--<li><a href="#">4</a></li>--}}
              {{--<li><a href="#">5</a></li>--}}
              {{--<li><a href="#">6</a></li>--}}
              {{--<li><a href="#">...</a></li>--}}
              {{--<li><a href="#">38</a></li>--}}
              {{--<li>--}}
                {{--<a aria-label="Next" href="#">--}}
                  {{--<span aria-hidden="true">Next</span>--}}
                {{--</a>--}}
              {{--</li>--}}
            {{--</ul>--}}

          </div><!-- about_inner -->
        </div><!-- course-grid -->
        @endif



        <div class="row clearfix">

          <div class="col-sm-8 col-xs-12">
            <div class="videoNine clearfix">

              <div class="videoArea clearfix" style="display: none">
                <h3>Welcome To Royal College</h3>
                <div class="row">
                  <div class="col-lg-8 col-md-7 col-xs-12 videoLeft">
                  <img src="img/home/video/video_image.jpg" data-video="https://www.youtube.com/embed/oOMcZoeEK0A?autoplay=1">
                  </div><!-- videoLeft -->
                  <div class="col-lg-4 col-md-5 col-xs-12 videoRight">
                  <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean consectetur ante volutpat sem aliquam lobortis. Mauris porta fermentum volutpat. Praesent est sapien, tincidunt vel arcu vitae, mattis sollicitudin lectus. Mauris porta fermentum volutpat. Praesent est sapien, tincidunt vel arcu vitae, mattis sollicitudin lectus.</p>
                  <a href="about.html" class="btn btn-block learnBtn">Learn More</a>
                  </div><!-- videoRight -->
                </div><!-- row -->
              </div><!-- videoArea -->

              {{--["nu_valorvenda"]=>--}}
              {{--string(7) "5499.00"--}}
              {{--["bl_mostrarpreco"]=>--}}
              {{--string(1) "0"--}}
              {{--["st_tituloexibicao"]=>--}}
              {{--string(26) "Economia no Setor Público"--}}
              {{--["id_projetopedagogico"]=>--}}


            @if(!empty($produtos))
            <div class="related_post_sec single_post">
              <h3>Cursos em destaque</h3>
              <ul>
                @foreach($produtos as $produto)
                <li>
                  {{--<span class="rel_thumb">--}}
                    {{--<a href="/curso/{{ $produto['id_projetopedagogico'] }}/{{ mb_strtolower($produto['st_produto'], 'UTF-8') }}"><img src="img/news/related_thumb_01.png" alt=""></a>--}}
                  {{--</span><!--end rel_thumb-->--}}
                  {{--<div class="rel_right">--}}
                  <div>
                    <h4><a href="/curso/{{ $produto['id_projetopedagogico'] }}/{{ mb_strtolower($produto['st_produto'], 'UTF-8') }}">{{ $produto['st_produto'] }}</a></h4>
                    <div class="meta">
                      Parcelas a partir de
                      <span class="author" style="color: green">R$  {{ number_format($produto['nu_valormensal'], 2, ',', '.') }}</span>
                    </div>
                    <p>{!! $produto['st_descricao'] !!}</p>
                    <a class="btn btn-info btn-matricule-se" href="/curso/{{ $produto['id_projetopedagogico'] }}/matricular" role="button">Matricule-se</a>
                  </div><!--end rel right-->
                </li>
                @endforeach
              </ul>
            </div><!--related_post_sec-->
            @endif



          </div><!--videoNine-->
          </div><!-- col-sm-8 col-xs-12 -->

          <div class="col-sm-4 col-xs-12">
            <div class="formArea clearfix">
              <div class="formTitle">
                <h3>Encontre seu Curso</h3>
                <p>O mercado de trabalho está cada vez mais exigente. Encontre agora o curso ideal para sua carreira!</p>
              </div><!-- formTitle -->
              <form action="/cursos" method="get">
                <div class="selectBox clearfix">
                  <select name="id_categoria" id="guiest_id2">
                    <option value="0"><i class="fa fa-align-justify" aria-hidden="true"></i>Todas Categorias</option>
                    @foreach($categorias as $categoria)
                      <option value="{{ $categoria['id_categoria'] }}">{{ $categoria['st_categoria'] }}</option>
                    @endforeach
                  </select>
                </div><!-- selectBox -->
                <div class="form-group">
                  <input type="text" class="form-control" name="st_texto" id="exampleInputEmail1" placeholder="Curso/área">
                </div>
                <button type="submit" class="btn btn-default btn-block commonBtn">Buscar</button>
              </form>
            </div><!-- formArea -->
            <div class="list_block related_post_sec" style="display: none">
              <div class="upcoming_events">
                <h3>Upcoming Events</h3>
                <ul>
                  <li class="related_post_sec single_post">
                    <span class="date-wrapper">
                      <span class="date"><span>24</span>January</span>
                    </span>
                    <div class="rel_right">
                      <h4><a href="single-events.html">Offered in small class sizes with great emphasis...</a></h4>
                      <div class="meta">
                        <span class="place"><i class="fa fa-map-marker"></i>Main Campus</span>
                        <span class="event-time"><i class="fa fa-clock-o"></i>11.00 pm</span>
                      </div>
                    </div>
                  </li>
                  <li class="related_post_sec single_post">
                    <span class="date-wrapper">
                      <span class="date"><span>24</span>January</span>
                    </span>
                    <div class="rel_right">
                      <h4><a href="single-events.html">Offered in small class sizes with great emphasis...</a></h4>
                      <div class="meta">
                        <span class="place"><i class="fa fa-map-marker"></i>Main Campus</span>
                        <span class="event-time"><i class="fa fa-clock-o"></i>11.00 pm</span>
                      </div>
                    </div>
                  </li>
                  <li class="related_post_sec single_post">
                    <span class="date-wrapper">
                      <span class="date"><span>24</span>January</span>
                    </span>
                    <div class="rel_right">
                      <h4><a href="single-events.html">Offered in small class sizes with great emphasis...</a></h4>
                      <div class="meta">
                        <span class="place"><i class="fa fa-map-marker"></i>Main Campus</span>
                        <span class="event-time"><i class="fa fa-clock-o"></i>11.00 pm</span>
                      </div>
                    </div>
                  </li>
                </ul>
                <a href="events-3col.html" class="btn btn-default btn-block commonBtn">More Events</a>
              </div>
            </div><!-- end list_block -->
          </div><!-- col-sm-4 col-xs-12 -->

        </div><!-- row clearfix -->
      </div><!-- container -->
    </div><!-- mainContent -->

    <div class="count clearfix wow fadeIn paralax" data-wow-delay="100ms" style="background-image: url(img/home/paralax/paralax03.jpg); display: none">
      <div class="container">
        <div class="row">
          <div class="col-xs-6 col-sm-3">
            <div class="text-center">
              <div class="icon"><i class="fa fa-group"></i></div>
              <div class="counter">
              <span class="timer">8298</span>
              </div>
              <div class="seperator-small"></div>
            <p>Students</p>
            </div>
          </div><!-- col-sm-3 -->
          <div class="col-xs-6 col-sm-3">
            <div class="text-center">
              <div class="icon"><i class="fa fa-book"></i></div>
              <div class="counter">
              <span class="timer">142</span>
              </div>
              <div class="seperator-small"></div>
              <p>Courses</p>
            </div>
          </div><!-- col-sm-3 -->
          <div class="col-xs-6 col-sm-3">
            <div class="text-center">
              <div class="icon"><i class="fa fa-male"></i></div>
              <div class="counter">
              <span class="timer">1047</span>
              </div>
              <div class="seperator-small"></div>
              <p>Stuffs</p>
            </div>
          </div><!-- col-sm-3 -->
          <div class="col-xs-6 col-sm-3">
            <div class="text-center">
              <div class="icon"><i class="fa fa-map-marker"></i></div>
              <div class="counter">
              <span class="timer">10</span>
              </div>
              <div class="seperator-small"></div>
              <p>Locations</p>
            </div>
          </div><!-- col-sm-3 -->
        </div><!-- row -->
        <div class="paralax-text text-center">
          <h2>Do you like this template?</h2>
          <p>nec congue consequat risus, nec volutpat enim tempus id. Proin et sapien eget diam ullamcorper consectetur. Sed blandit imperdiet mauris. Mauris eleifend faucib</p>
          <p>ipsum quis varius. Quisque pharetra leo erat, non eleifend nibh interdum quis.</p>
          <a href="buying-steps.html" class="btn btn-default commonBtn">Buy now</a>
        </div><!-- row -->
      </div><!-- container -->
    </div><!-- count -->

    <div class="testimonial-section clearfix">
      <div class="container">
        <div class="row">
          {{--<div class="col-xs-12 col-sm-6">--}}
            {{--<div class="testimonial">--}}
              {{--<div class="carousal_content">--}}
                {{--<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris</p>--}}
              {{--</div>--}}
              {{--<div class="carousal_bottom">--}}
                {{--<div class="thumb">--}}
                  {{--<img src="img/about/SARA-LISBON_Art-Student.jpg" alt="" draggable="false">--}}
                {{--</div>--}}
                {{--<div class="thumb_title">--}}
                  {{--<span class="author_name">Sara Lisbon</span>--}}
                  {{--<span class="author_designation">Student<a href="#"> English Literature</a></span>--}}
                {{--</div>--}}
              {{--</div>--}}
            {{--</div><!-- testimonial -->--}}
          {{--</div><!-- col-xs-12 -->--}}
          <div class="col-xs-12 col-sm-12">
            <div class="features">
              <h3>Porque a UCAM?</h3>
              <ul>
                <li><i class="fa fa-check-circle-o"></i>Somos a mais antiga e tradicional Universidade particular do país. </li>
                <li><i class="fa fa-check-circle-o"></i>Somos comprometidos com a excelência nas atividades de pesquisa e pós-graduação.</li>
                <li><i class="fa fa-check-circle-o"></i>Temos centros e institutos de pesquisa, sete programas de mestrado e dois de doutorado, bem como dezenas de cursos de pós-graduação lato sensu.</li>
                <li><i class="fa fa-check-circle-o"></i>Nossos cursos de pós-graduação são todos reconhecidos pelo MEC conforme Portaria do MEC nº 1.282, de 26 de outubro de 2010.</li>
                <li><i class="fa fa-check-circle-o"></i>Fazer uma pós-graduação, segundo a 52ª edição da Pesquisa Salarial e de Benefícios realizada pela Catho, é uma importante estratégia para recolocar-se no mercado ou até mesmo elevar os salários em 50% nas mais diversas áreas.</li>
              </ul>
            </div>
          </div><!-- col-xs-12 -->
        </div><!-- row -->
      </div><!-- container -->
    </div><!-- testimonial-section -->

    <div class="brandSection clearfix" style="display: none">
      <div class="container">
        <div class="row">
          <div class="col-xs-12">
            <div class="owl-carousel partnersLogoSlider">
              <div class="slide">
                <div class="partnersLogo clearfix">
                  <a href="#"><img src="img/home/brand1.png" /></a>
                </div>
              </div>
              <div class="slide">
                <div class="partnersLogo clearfix">
                  <a href="#"><img src="img/home/brand2.png" /></a>
                </div>
              </div>
              <div class="slide">
                <div class="partnersLogo clearfix">
                  <a href="#"><img src="img/home/brand3.png" /></a>
                </div>
              </div>
              <div class="slide">
                <div class="partnersLogo clearfix">
                  <a href="#"><img src="img/home/brand4.png" /></a>
                </div>
              </div>
              <div class="slide">
                <div class="partnersLogo clearfix">
                  <a href="#"><img src="img/home/brand5.png" /></a>
                </div>
              </div>
              <div class="slide">
                <div class="partnersLogo clearfix">
                  <a href="#"><img src="img/home/brand1.png" /></a>
                </div>
              </div>
              <div class="slide">
                <div class="partnersLogo clearfix">
                  <a href="#"><img src="img/home/brand2.png" /></a>
                </div>
              </div>
              <div class="slide">
                <div class="partnersLogo clearfix">
                  <a href="#"><img src="img/home/brand3.png" /></a>
                </div>
              </div>
              <div class="slide">
                <div class="partnersLogo clearfix">
                  <a href="#"><img src="img/home/brand4.png" /></a>
                </div>
              </div>
              <div class="slide">
                <div class="partnersLogo clearfix">
                  <a href="#"><img src="img/home/brand5.png" /></a>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div><!-- Brand-section -->

@include('footer')