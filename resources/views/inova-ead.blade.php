@include('header')
<!-- ERROR CONTENT -->
<div class="mainContent clearfix">
    <div class="single_banner">
        <div class="container">
            <div class="single_banner_inner" style="min-height: 200px; position: relative">
                <img src="/img/banner-inovaead-3.jpg" alt=""/>
            </div><!--end single_banner_inner-->
        </div><!--end container-->
    </div><!--end single banner-->
    <div class="container">
        <h2><br>Inova EaD<br><br></h2>

        <p>O INOVA EaD é um Programa de Pós-Graduação concebido para a aplicação e desenvolvimento de novas tecnologias educacionais, adaptadas ao novo milênio, na modalidade a distância, que preserva a tradição e qualidade dos programas presenciais de ensino e pesquisa da UCAM, bem como traz a excelência do seu corpo docente.</p>

        <p><strong>Coordenado por Mestres e Doutores</strong> de diferentes áreas do saber, seu corpo docente possui um caráter multidisciplinar voltado para a realidade do mercado.

        <p>Desenvolvido em um ambiente <strong>totalmente on-line</strong>, colaborativo, com a utilização de diversas mídias, recursos e funcionalidades, o nosso estudante faz parte de uma comunidade de aprendizado que permite a troca de conhecimento entre os participantes dos cursos, disciplinas ou atividades que realiza, proporcionando um modelo pedagógico que torna o processo ensino-aprendizagem adequado ao desenvolvimento dos alunos, no seu tempo e na sua hora.</p>

        <p>Iniciado como programa EaD com cursos de História da África e do Negro no Brasil, o <strong>INOVA EaD</strong> amplia a abrangência da UCAM, com os cursos nas área do Direito, Administração e Gestão Pública, Educação e Magistério, MBA e Negócio, Saúde e Bem-Estar e Segurança e Inteligência, com conteúdos atuais e totalmente voltados para o mercado de trabalho, atingindo as áreas acadêmicas em que se destaca, mantendo a tradição e a excelência que fazem dessa, desde 1902, a mais <strong>antiga, inovadora, e ao mesmo tempo tradicional, instituição de ensino privado do Brasil.</strong></p>

        <p>Os cursos de Pós-Graduação Lato Sensu oferecidos estão de acordo com a Resolução nº 01, de 08/07/2007 do MEC/CNE, sendo que nossos cursos são <strong>autorizados pela Portaria do MEC n°. 1.282, de 26 de outubro de 2010.</strong></p>


    </div>
</div>
@include('footer')

