<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/obrigado', 'Web\DynamicController@obrig');
Route::get('/', 'Web\IndexController@index');
Route::get('/pagina-nao-encontrada', 'Web\DynamicController@notFound');
Route::get('/inova-ead', 'Web\IndexController@inovaEad');
Route::get('/cursos', 'Web\CourseController@index');
Route::get('/curso/{id_projetopedagogico}/matricular', 'Web\CourseController@courseInscription');
Route::get('/curso/{id_projetopedagogico}/{st_produto}', 'Web\CourseController@courseDetails');

Route::get('/alta-performance', 'Web\AltaController@index');


Route::get('/entre-em-contato', 'Web\ContactController@index');
Route::get('/contato', 'Web\ContactController@index');
Route::get('/contato/{id_produto}/{st_produto}', 'Web\ContactController@index');
Route::get('/fale-conosco', 'Web\ContactController@index');
Route::post('/enviando-mensagem', 'Web\ContactController@send');
Route::get('/categoria/{id_categoria}/{st_categoria}', 'Web\CourseController@category');

Route::get('/welcome', function () {
    return view('welcome');
});

//Route::get('/alta-performance', function(){
//    return view('alta-performance');
//});

Route::get('/teste', function(){
    return view('teste');
});


Route::get('{page_name}', 'Web\DynamicController@index');